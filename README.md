# endlessdepth

A very simple text-based game.

---

## Stability

It's not yet.

Move along, nothing to see here.

---

## Installing

NOT YET IMPLEMENTED.

---

## Compiling

### Configuring

If you wish to change keybindings, then you need to modify [config.h](src/config.h).

It's fairly straightforward.

---

### Compiling

You need ncurses to be installed first.

Then you need a C-compiler, and make installed.

Finally, run:

```
make
```

You should get a binary called 'endlessdepth' out at the end.

---

## LICENSE

Licensed under Creative Commons, Zero License.

See [LICENSE](LICENSE) for the exact legal code.
