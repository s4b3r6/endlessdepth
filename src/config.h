#ifndef ENDLESSDEPTH_CONFIG

#define keyup 'w'
#define keydown 's'
#define keyleft 'a'
#define keyright 'd'
#define keyquit 'q'
#define keyattack 'e'
#define savekey 's'
#define loadkey 'l'

#define ENDLESSDEPTH_CONFIG
#endif
