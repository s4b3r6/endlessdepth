#include <ncurses.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#include "config.h"

struct player {
  uint32_t x;
  uint16_t pickaxe;
  uint32_t y;
  uint16_t dir;
  uint64_t kills;
  uint16_t weapon;
  uint64_t gold;
  uint32_t health;
};

void init_player(struct player* p) {
  p->x = 1;
  p->y = 1;
  p->pickaxe = 1;
  p->dir = 0;
  p->kills = 0;
  p->weapon = 1;
  p->gold = 0;
  p->health = 100;
}

void draw_player(struct player* p) {
  char player_char;
  switch(p->dir) {
    case(0):
      player_char = 'v';
      break;
    case(1):
      player_char = '^';
      break;
    case(2):
      player_char = '<';
      break;
    case(3):
      player_char = '>';
      break;
    default:
      player_char = 'v';
      break;
  }
  mvprintw(p->y, p->x, "%c", player_char);
}

struct world {
  uint32_t width;
  uint32_t height;
  uint32_t treasure_x;
  uint32_t treasure_y;
  bool treasure_exists;
  uint32_t exit_x;
  uint32_t exit_y;
  bool monsterA_alive;
  uint32_t monsterA_x;
  uint32_t monsterA_y;
  bool monsterB_alive;
  uint32_t monsterB_x;
  uint32_t monsterB_y;
  bool monsterC_alive;
  uint32_t monsterC_x;
  uint32_t monsterC_y;
};

int rand_range(int min, int max) {
  return rand() % (max + 1 - min) + min;
}

void init_world(struct world* w) {
  w->width = COLS;
  w->height = LINES - 1;
  w->exit_x = COLS - 2;
  w->exit_y = LINES - 1;
  w->treasure_x = rand_range(2, COLS - 2);
  w->treasure_y = rand_range(2, LINES - 2);
  w->treasure_exists = 1;
  w->monsterA_alive = 1;
  w->monsterA_x = rand_range(2, COLS - 2);
  w->monsterA_y = rand_range(2, LINES - 2);
  w->monsterB_alive = 1;
  w->monsterB_x = rand_range(2, COLS - 2);
  w->monsterB_y = rand_range(2, LINES - 2);
  w->monsterC_alive = 1;
  w->monsterC_x = rand_range(2, COLS - 2);
  w->monsterC_y = rand_range(2, LINES - 2);
}

void draw_scores(struct player* p) {
  mvprintw(0,0,"H:%"PRIu32" G:%"PRIu64" K:%"PRIu64" W:%"PRIu16" P:%"PRIu16, p->health, p->gold, p->kills, p->weapon, p->pickaxe);
}

void draw_world(struct world* w) {
  for(int i = 0; i < w->width; i++) {
  // Top row
    mvprintw(0, i, "%c", '+');
  // Bottom row
    mvprintw(w->height, i, "%c", '+');
  // Left wall
    mvprintw(i, 0, "%c", '+');
  // Right wall
    mvprintw(i, w->width - 1, "%c", '+');
  }
  // Treasure
  if(w->treasure_exists)
    mvprintw(w->treasure_y, w->treasure_x, "%c", '*');
  // Exit
  mvprintw(w->exit_y, w->exit_x, "%c", 'E');
  // Monsters
  if(w->monsterA_alive)
    mvprintw(w->monsterA_y, w->monsterA_x, "%c", '@');
  if(w->monsterB_alive)
    mvprintw(w->monsterB_y, w->monsterB_x, "%c", '#');
  if(w->monsterC_alive)
    mvprintw(w->monsterC_y, w->monsterC_x, "%c", '%');
}

// TODO: random rocks... Must be attackable.
// TODO: void move_monsters(struct world* w) {}

// TODO: void player_action(struct player* p, struct world* w) {}
// Kill monsters
// Break rocks (Random chance of lil reward)
// Get treasure
// Exit map

// TODO: void save_game(struct player* p, struct world* w) {}
// TODO: void load_load(struct player* p, struct world* w) {}
// TODO: void player_exit(struct player* p, struct world* w) {}
// TODO: void player_reward(struct player* p) {}

int main(int argc, char* argv[]) {
  initscr();
  noecho();
  cbreak();
  curs_set(0);

  struct player p;
  struct world w;
  time_t t;

  srand((unsigned) time(&t));

  init_player(&p);
  init_world(&w);

  while(1) {
    draw_world(&w);
    draw_player(&p);
    draw_scores(&p);
    refresh();

    int key = getch();
    //keys defined in config.h
    if(key == keyup && p.y > 1) {
      mvprintw(p.y, p.x, "%c", ' ');
      p.y--;
      p.dir = 1;
    } else
    if(key == keydown && p.y < LINES - 2) {
      mvprintw(p.y, p.x, "%c", ' ');
      p.y++;
      p.dir = 0;
    } else
    if(key == keyright && p.x < COLS - 2) {
      mvprintw(p.y, p.x, "%c", ' ');
      p.x++;
      p.dir = 3;
    } else
    if(key == keyleft && p.x > 1) {
      mvprintw(p.y, p.x, "%c", ' ');
      p.x--;
      p.dir = 2;
    } else
    if(key == keyquit) {
      endwin();
      return 0;
    }
  }
  endwin();
}
