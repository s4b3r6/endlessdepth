CFLAGS=-Wall -std=c11

all: main

main: src/main.c
	$(CC) $(CFLAGS) -Isrc src/main.c -lncurses -o endlessdepth
